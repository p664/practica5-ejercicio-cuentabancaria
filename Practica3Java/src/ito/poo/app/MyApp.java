package ito.poo.app;
import java.time.LocalDate;
import ito.CuentaBancaria;

public class MyApp {

	public static void main(String[] args) {
		
		CuentaBancaria c=new CuentaBancaria(1234, "Ximena Trujillo Perez", 1000f, LocalDate.of(2021, 10, 8));
	    System.out.println(c);
	    
	    System.out.println(c.retiro(500f, LocalDate.of(2021, 10, 11)));
	    System.out.println(c);
	    System.out.println(c.deposito(200f, LocalDate.of(2021, 10, 13)));
	    System.out.println(c); 
	    
	}
}
